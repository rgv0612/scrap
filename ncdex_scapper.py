#!/usr/bin/python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import sqlalchemy
from sqlalchemy import create_engine, event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData
from sqlalchemy import Table, Column, Integer, String, Float, MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import CreateTable

import numpy as np
import pandas as pd
import os
import datetime
import re
import pymysql
import time

# for python3
import urllib.request

user = "root"
pw = "root"
host = "localhost"
db = "mandi"
table_name = "scapped_ncdex_data"

engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user=user,
                               pw=pw,
                               db=db))

Base = declarative_base()
metadata = MetaData(engine, reflect=True)

# Declaration of the class in order to write into the database. This structure is standard and should align with SQLAlchemy's doc.


class Current(Base):
    __tablename__ = 'scapped_ncdex_data'

    id = Column(Integer, primary_key=True)
    Product = Column(String(500))
    Expiry = Column(String(500))
    OpenP = Column(Float())
    High = Column(Float())
    Low = Column(Float())
    CloseP = Column(Float())
    LTP = Column(Float())
    ChangeP = Column(Float())
    PerChange = Column(Float())
    Avtp = Column(Float())
    SpotP = Column(Float())
    SpotDt = Column(String(500))
    BestB = Column(Float())
    BestS = Column(Float())
    Oi = Column(Integer())
    TStamp = Column(String(50))

    def __repr__(self):
        return
        "(id='%d', Product='%s', Expiry='%s', OpenP='%f', High='%f', Low='%f', CloseP='%f', LTP='%f', ChangeP='%f', PerChange='%f', Avtp='%f', SpotP='%f', SpotDt='%s', BestB='%f', BestS='%f', Oi='%d')" % (
            self.id, self.Product, self.Expiry, self.OpenP, self.High, self.Low, self.CloseP, self.LTP, self.ChangeP, self.PerChange, self.Avtp, self.SpotP, self.SpotDt, self.BestB, self.BestS, self.Oi)


def delete_all_csvs():
    mydir = os.getcwd()
    filelist = [f for f in os.listdir(mydir) if f.endswith(".csv")]
    for f in filelist:
        os.remove(os.path.join(mydir, f))
        print("deleting ", os.path.basename(f))


def remove_table(engine, table_name):
    base = declarative_base()
    metadata = MetaData(engine, reflect=True)
    table = metadata.tables.get(table_name)
    if table is not None:
        print('Deleting {table_name} table')
        base.metadata.drop_all(engine, [table], checkfirst=True)


def scrap_data_from_ncdex():
    url = 'https://ncdex.com/MarketData/LiveFuturesQuotes.aspx'

    with urllib.request.urlopen(url) as url:
        html = url.read()

    soup = BeautifulSoup(html, 'lxml')
    table = soup.find('table', id='Table1')
    rows = table.findChildren(['tr'])
    row = rows[0]
    cells = row.findChildren('th')
    print()
    header = ''

    regex = re.compile('[^ a-zA-Z0-9.,:-]')

    for cell in cells:
        value = str(regex.sub('', cell.text)).strip()
        if value:
            header += value + ','
    header = header + '\n'
    datatable = soup.find('table',
                          id='ctl00_ContentPlaceHolder3_dgLiveFuturesQuotes'
                          )
    datarows = datatable.findChildren('tr')
    for row in datarows[1:]:
        cells = row.findChildren('td')
        if cells:
            for cell in cells:
                if cell.text:
                    value = str(regex.sub('', cell.text)).strip()
                    if value:
                        header = header + value + ','
                else:
                    print('empty cell')
        header = header + '\n'

    headers = 'attachment; filename=data_' \
        + datetime.datetime.now().strftime('%Y_%m_%d %H_%M_%S') + '.csv'

    delete_all_csvs()

    filename = datetime.datetime.now().strftime('%Y_%m_%d %H_%M_%S') + ".csv"
    f = open(filename, "a")
    f.write(header)
    f.close()

    # read csv then create dataframe
    df = pd.read_csv(filename, skiprows=1)
    df = df.iloc[:, :-1]

    # delete old data & create new table
    remove_table(engine, table_name)
    Base.metadata.create_all(
        engine, Base.metadata.tables.values(), checkfirst=True)

    # #print table schema
    # print(CreateTable(Current.__table__))

    metadata = sqlalchemy.schema.MetaData(bind=engine, reflect=True)
    table = sqlalchemy.Table(table_name, metadata, autoload=True)

    # Open the session
    Session = sessionmaker(bind=engine)
    session = Session()

    for index, row in df.iterrows():
        current = Current(
            Product=df.iat[index, 0],
            Expiry=df.iat[index, 1],
            OpenP=float(df.iat[index, 2]),
            High=float(df.iat[index, 3]),
            Low=float(df.iat[index, 4]),
            CloseP=float(df.iat[index, 5]),
            LTP=float(df.iat[index, 6]),
            ChangeP=float(df.iat[index, 7]),
            PerChange=float(df.iat[index, 8]),
            Avtp=float(df.iat[index, 9]),
            SpotP=float(df.iat[index, 10]),
            SpotDt=df.iat[index, 11],
            BestB=float(df.iat[index, 12]),
            BestS=float(df.iat[index, 13]),
            Oi=int(df.iat[index, 14]),
            TStamp=float(time.time() - 5*60))
        session.add(current)

    # Commit the changes
    session.commit()

    # Close the session
    session.close()

    # #Insert whole DataFrame into MySQL
    #df.to_sql(table_name, con = engine, if_exists = 'append', chunksize = 1000)

    print('Reached to the end')


scrap_data_from_ncdex()
