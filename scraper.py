
from sqlalchemy.schema import CreateTable
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table, Column, Integer, String, Float, MetaData
from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, event
import sqlalchemy
import time
import numpy as np
from flask import Flask, Response
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import datetime
from selenium.webdriver.chrome.options import Options
import pandas as pd
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--no-sandbox")

user = "root"
pw = "root"
db = "mandi"
table_name = "scapped_rtncv2_data"

# Database Connection
engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                       .format(user=user,
                               pw=pw,
                               db=db))


# Model for storing scrapped data
Base = declarative_base()


class Current(Base):
    __tablename__ = 'scapped_rtncv2_data'

    id = Column(Integer, primary_key=True)
    Script = Column(String(500))
    Expiry = Column(String(500))
    Buy = Column(Float())
    Sell = Column(Float())
    LTP = Column(Float())
    NetChng = Column(Float())
    Chng = Column(Float())
    Open = Column(Float())
    High = Column(Float())
    Low = Column(Float())
    Close = Column(Float())
    TotBQty = Column((Float))
    TotSQty = Column(Float())
    TStamp = Column(String(50))

    def __repr__(self):
        return
        "(id='%d', Script='%s', Expiry='%s', Buy='%f', Sell='%f', LTP='%f', NetChng='%f', Chng='%f', Open='%f', High='%f', Low='%f', TotBQty='%f', TotSQty='%f')" % (
            self.id, self.Script, self.Expiry, self.Buy, self.Sell, self.LTP, self.NetChng, self.Chng, self.Open, self.High, self.Low, self.TotBQty, self.TotSQty)

# Scrap data


def scrap():
    url = "http://95.216.2.220/com/24rate/rtncv2.html"

    browser = webdriver.Chrome(
        executable_path="C:\\Users\\Dell\\Desktop\\New folder (7)\\chromedriver", chrome_options=chrome_options)
    browser.implicitly_wait(10)
    browser.get(url)

    myDynamicElement = browser.find_element_by_class_name("bg-info")
    html = browser.page_source

    soup = BeautifulSoup(html, 'lxml')
    table = soup.find(id="datatable3")

    rows = table.findChildren(['tr'])

    row = rows[0]
    cells = row.findChildren('th')
    print()
    header = ""
    for cell in cells:
        header = header + cell.string + ','

    header = header + "\n"

    for row in rows:
        cells = row.findChildren('td')
        for cell in cells:
            header = header + cell.string + ','

        if cells:
            header = header + "\n"

    browser.quit()
    #headers = "attachment; filename=data_live_" + datetime.datetime.now().strftime('%Y_%m_%d %H_%M_%S') + ".csv"

    filename = "live" + datetime.datetime.now().strftime('%Y_%m_%d %H_%M_%S') + ".csv"
    f = open(filename, "a")
    f.write(header)
    f.close()

    # read csv then create dataframe
    df = pd.read_csv(filename, skiprows=1)
    df = df.iloc[:, :-1]
    pd.set_option('display.max_rows', 1000)

    print(df.head())

    # Updation by recreating the same table and adding data
    remove_table(engine, table_name)
    Base.metadata.create_all(
        engine, Base.metadata.tables.values(), checkfirst=True)

    metadata = sqlalchemy.schema.MetaData(bind=engine, reflect=True)
    table = sqlalchemy.Table(table_name, metadata, autoload=True)

    # Open the session
    Session = sessionmaker(bind=engine)
    session = Session()

    for index, row in df.iterrows():
        current = Current(
            Script=df.iat[index, 0],
            Expiry=df.iat[index, 1],
            Buy=float(df.iat[index, 2]),
            Sell=float(df.iat[index, 3]),
            LTP=float(df.iat[index, 4]),
            NetChng=float(df.iat[index, 5]),
            Chng=float(df.iat[index, 6]),
            High=float(df.iat[index, 7]),
            Low=float(df.iat[index, 8]),
            Close=float(df.iat[index, 9]),
            TotBQty=float(df.iat[index, 10]),
            TotSQty=float(df.iat[index, 11]),
            TStamp=time.time()-5*60)
        session.add(current)

    # Commit the changes
    session.commit()

    # Close the session
    session.close()

    # #Insert whole DataFrame into MySQL
    #df.to_sql(table_name, con = engine, if_exists = 'append', chunksize = 1000)

    print('Database updated successfully')


# Remove table on update
def remove_table(engine, table_name):
    base = declarative_base()
    metadata = MetaData(engine, reflect=True)
    table = metadata.tables.get(table_name)
    if table is not None:
        print('Deleting {table_name} table')
        base.metadata.drop_all(engine, [table], checkfirst=True)

# Delete all csv files


def delete_all_csvs():
    mydir = os.getcwd()
    filelist = [f for f in os.listdir(mydir) if f.endswith(".csv")]
    for f in filelist:
        os.remove(os.path.join(mydir, f))
        print("deleting ", os.path.basename(f))


# Scrap the data from the site
scrap()

# Delete csv files after db is updated
delete_all_csvs()
